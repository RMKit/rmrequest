//
//  RMAppDelegate.h
//  RMRequest
//
//  Created by Rain on 09/11/2023.
//  Copyright (c) 2023 Rain. All rights reserved.
//

@import UIKit;

@interface RMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
