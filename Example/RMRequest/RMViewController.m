//
//  RMViewController.m
//  RMRequest
//
//  Created by Rain on 09/11/2023.
//  Copyright (c) 2023 Rain. All rights reserved.
//

#import "RMViewController.h"
#import <RMRequest/RMHttpRequest.h>

@interface RMViewController ()

@end

@implementation RMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Request request:Msg.xroute(@"") withResponseBlock:^(RMRequestMessage * _Nonnull request, id  _Nonnull responseData, NSError * _Nonnull error) {
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
