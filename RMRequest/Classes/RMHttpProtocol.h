//
//  RMHttpProtocol.h
//  RMNetRequest
//
//  Created by RainGu on 2021/4/21.
//

#ifndef RMHttpProtocol_h
#define RMHttpProtocol_h

#import <AFNetworking/AFNetworking.h>
#import "RMHttpEndEvent.h"
#import "RMHttpErrorEvent.h"
@class  RMHttpRequest;
@protocol RMHandleEventDelegate <NSObject>

@optional

- (void)handleHttpEndEvent:(RMHttpEndEvent *_Nonnull)event;

- (void)handleHttpErrorEvent:(RMHttpErrorEvent *_Nonnull)event;

- (void)handleHttpLocalEvent:(RMHttpEndEvent *_Nonnull)event;

@end

@protocol  RMHttpRequestDelegate<NSObject>
@optional

- (NSDictionary *_Nullable)requestHeaderFile:(RMHttpRequest *_Nullable)request;

-(AFHTTPRequestSerializer *_Nullable)requestSerializerFile:(RMHttpRequest *_Nullable)request;

- (RMRequestMessage *_Nullable)handleRequestMessage:(RMRequestMessage *_Nullable)message;

@required

@end

#endif /* RMHttpProtocol_h */
