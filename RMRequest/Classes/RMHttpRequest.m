//
//  RMHttpRequest.m
//  RMNetRequest
//
//  Created by RainGu on 2021/4/20.
//

#import "RMHttpRequest.h"
#import <pthread/pthread.h>

@interface RMHttpRequest()

@end

@implementation RMHttpRequest

+(instancetype)shareInstance{
    RMHttpRequest *net = [[RMHttpRequest alloc] init];
    return net;
}

- (instancetype)init {
    if (self = [super init]) {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
        // 设置请求格式
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.manager = manager;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:
                                                             @"application/json",
                                                             @"application/xml",
                                                             @"text/json",
                                                             @"text/javascript",
                                                             @"text/html",
                                                             @"text/plain", nil];
        dispatch_queue_t _processingQueue = dispatch_queue_create("com.yuantiku.networkagent.processing", DISPATCH_QUEUE_CONCURRENT);
        NSIndexSet *_allStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(100, 500)];
        
        pthread_mutex_t _lock;
        pthread_mutex_init(&_lock, NULL);

        self.manager.responseSerializer.acceptableStatusCodes = _allStatusCodes;
        self.manager.completionQueue = _processingQueue;
    }
    return self;
}

+(RMHttpRequest *)request:(RMRequestMessage *)message withResponseBlock:(RMHttpResponseBlock)responseBlock{
    RMHttpRequest *net = [RMHttpRequest shareInstance];
    [net request:message withResponseBlock:responseBlock];
    return net;
}

-(RMHttpRequest *)request:(RMRequestMessage *)message withResponseBlock:(RMHttpResponseBlock)responseBlock{
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHttpRequestDelegate"]){
        self.reqDelegate =NSClassFromString([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHttpRequestDelegate"]).new;
    }
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHandleEventDelegate"]){
        self.evnentDelegate = NSClassFromString([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHandleEventDelegate"]).new;
    }
    self.requestMessage = message;
    self.responseBlock = responseBlock;
    [self.responseBlock copy];
    [self configRequstManager];
    switch (message.type) {
        case  HttpRequestDefault:
            [self defaultRequest:message withProgress:nil withResponseBlock:responseBlock];
            break;
        case HttpRequestUploadFile:
            [self  uploadFileRequest:message withProgress:nil withResponseBlock:responseBlock];
            break;
        case HttpRequestDownloadFile:
            [self downloadFileRequest:message withProgress:nil withResponseBlock:responseBlock];
            break;
        default:
            break;
    }
    return self;
}

+ (RMHttpRequest *)request:(RMRequestMessage *)message withProgress:(RMHttpRequestProgress)progressBlcok withResponseBlock:(RMHttpResponseBlock)responseBlock{
    RMHttpRequest *net = [RMHttpRequest shareInstance];
    [net request:message withProgress:progressBlcok withResponseBlock:responseBlock];
    return net;
}

- (RMHttpRequest *)request:(RMRequestMessage *)message withProgress:(RMHttpRequestProgress)progressBlcok withResponseBlock:(RMHttpResponseBlock)responseBlock{
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHttpRequestDelegate"]){
        self.reqDelegate =NSClassFromString([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHttpRequestDelegate"]).new;
    }
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHandleEventDelegate"]){
        self.evnentDelegate = NSClassFromString([[NSUserDefaults standardUserDefaults]objectForKey:@"RMHandleEventDelegate"]).new;
    }
    self.requestMessage = message;
    self.responseBlock = responseBlock;
    [self.responseBlock copy];
    self.progressBlock = progressBlcok;
    [self configRequstManager];
    switch (message.type) {
        case  HttpRequestDefault:
            [self defaultRequest:message withProgress:progressBlcok withResponseBlock:responseBlock];
            break;
        case HttpRequestUploadFile:
            [self uploadFileRequest:message withProgress:progressBlcok withResponseBlock:responseBlock];
            break;
        case HttpRequestDownloadFile:
            [self downloadFileRequest:message withProgress:progressBlcok withResponseBlock:responseBlock];
            break;
        default:
            break;
    }
    return self;
}

-(void)configRequstManager{
    if([self.reqDelegate respondsToSelector:@selector(requestHeaderFile:)]) {
    NSDictionary *dic = [self.reqDelegate requestHeaderFile:self];
    for (NSString *key in dic.allKeys) {
        [self.manager.requestSerializer setValue:[dic objectForKey:key] forHTTPHeaderField:key];
     }
   }
   if ([self.reqDelegate respondsToSelector:@selector(requestSerializerFile:)]) {
      self.manager.requestSerializer =  [self.reqDelegate requestSerializerFile:self];
    }
}

-(void)defaultRequest:(RMRequestMessage *)message withProgress:(RMHttpRequestProgress)progressBlcok withResponseBlock:(RMHttpResponseBlock)responseBlock{
  WeakifySelf()
  if (message.isNotLocalData) {
      if ([self.evnentDelegate respondsToSelector:@selector(handleHttpLocalEvent:)]) {
           RMHttpEndEvent *event = RMHttpEndEvent.new;
           event.requstMessage   = message;
           event.httpRequst      =  self;
           [self.evnentDelegate handleHttpEndEvent:event];
          return;
        }
    }
    if([self.reqDelegate respondsToSelector:@selector(handleRequestMessage:)]){
        message = [self.reqDelegate handleRequestMessage:message];
    }
    NSURLSessionDataTask *task=  [self.manager dataTaskWithHTTPMethod:[self getMethod:message] URLString:message.route parameters:message.params headers:message.headers uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
   } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
       if (progressBlcok) {
           progressBlcok(downloadProgress);
       }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([self.evnentDelegate respondsToSelector:@selector(handleHttpEndEvent:)]) {
           RMHttpEndEvent *event = RMHttpEndEvent.new;
           event.requstMessage   = message;
           event.responsData     = responseObject;
           event.httpRequst      =  weakSelf;
           weakSelf.responseBlock = responseBlock;
           weakSelf.requestMessage = message;
          [weakSelf.evnentDelegate handleHttpEndEvent:event];
        }else{
            NSError *err ;
            responseBlock(message,responseObject,err);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if ([self.evnentDelegate respondsToSelector:@selector(handleHttpErrorEvent:)]) {
          RMHttpErrorEvent *evnet = RMHttpErrorEvent.new;
           evnet.error = error;
           evnet.requstMessage = message;
           evnet.httpRequst = weakSelf;
           weakSelf.responseBlock = responseBlock;
           weakSelf.requestMessage = message;
          [weakSelf.evnentDelegate handleHttpErrorEvent:evnet];
        }else{
            responseBlock(message,@"failuer",error);
        }
    }];
    [task resume];
}

-(void)uploadFileRequest:(RMRequestMessage *)message withProgress:(RMHttpRequestProgress)progressBlcok withResponseBlock:(RMHttpResponseBlock)responseBlock{
   WeakifySelf()
    if([self.reqDelegate respondsToSelector:@selector(handleRequestMessage:)]){
        message = [self.reqDelegate handleRequestMessage:message];
    }
   NSURLSessionDataTask * task = [self.manager POST:message.route parameters:message.params headers:message.headers constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
     
     [formData appendPartWithFileData:message.upFile.fileData name:message.upFile.Name fileName:message.upFile.fileName mimeType:message.upFile.mimeType];
     
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            if (progressBlcok) {
                progressBlcok(uploadProgress);
            }
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([self.evnentDelegate respondsToSelector:@selector(handleHttpEndEvent:)]) {
               RMHttpEndEvent *event = RMHttpEndEvent.new;
               event.requstMessage   = message;
               event.responsData     = responseObject;
               event.httpRequst      =  weakSelf;
               weakSelf.responseBlock = responseBlock;
                weakSelf.requestMessage = message;
               [weakSelf.evnentDelegate handleHttpEndEvent:event];
            }else{
               NSError *err ;
               responseBlock(message,responseObject,err);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if ([self.evnentDelegate respondsToSelector:@selector(handleHttpErrorEvent:)]) {
              RMHttpErrorEvent *evnet = RMHttpErrorEvent.new;
              evnet.error = error;
              evnet.requstMessage = message;
              evnet.httpRequst = weakSelf;
              weakSelf.responseBlock = responseBlock;
              weakSelf.requestMessage = message;
              [weakSelf.evnentDelegate handleHttpErrorEvent:evnet];
            }else{
              responseBlock(message,@"failure",error);
            }
        }];
        [task resume];
}

-(void)downloadFileRequest:(RMRequestMessage *)message withProgress:(RMHttpRequestProgress)progressBlcok withResponseBlock:(RMHttpResponseBlock)responseBlock{
    WeakifySelf()
    if([self.reqDelegate respondsToSelector:@selector(handleRequestMessage:)]){
        message = [self.reqDelegate handleRequestMessage:message];
    }
    NSURLSessionDownloadTask *task = [self.manager downloadTaskWithRequest:[RMHttpRequest getUrlRequest:message.route] progress:^(NSProgress * _Nonnull downloadProgress) {
        if (progressBlcok) {
            progressBlcok(downloadProgress);
        }
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        //设置下载路径
        return [NSURL fileURLWithPath:message.downFilePath];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        responseBlock(message,@"downLoad Successfull",error);
    }];
     [task resume];
}

-(NSString *)getMethod:(RMRequestMessage *)message{
    switch (message.method) {
            case RMRequestMethodGET:    return @"GET";
            case RMRequestMethodPOST:   return @"POST";
            case RMRequestMethodPUT:    return @"PUT";
            case RMRequestMethodDELETE: return @"DELETE";
            case RMRequestMethodPATCH:  return @"PATCH";
            case RMRequestMethodHEAD:   return @"HEAD";
            default:NSCAssert(NO, @"无效的HTTPMethod");
    }
}

+(NSURLRequest *)getUrlRequest:(NSString *)route{
    NSURLRequest *requst = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:route]];
    return requst;
}

+ (void)setRequestDelegate:(id<RMHttpRequestDelegate>)delegate {
    [[NSUserDefaults standardUserDefaults]setObject:delegate forKey:@"RMHttpRequestDelegate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)setHandleDelegate:(id<RMHandleEventDelegate>)delegate{
    [[NSUserDefaults standardUserDefaults]setObject:delegate forKey:@"RMHandleEventDelegate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
