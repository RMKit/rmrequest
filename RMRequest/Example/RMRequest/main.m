//
//  main.m
//  RMRequest
//
//  Created by Rain on 09/11/2023.
//  Copyright (c) 2023 Rain. All rights reserved.
//

@import UIKit;
#import "RMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RMAppDelegate class]));
    }
}
