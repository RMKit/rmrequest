# RMRequest

[![CI Status](https://img.shields.io/travis/Rain/RMRequest.svg?style=flat)](https://travis-ci.org/Rain/RMRequest)
[![Version](https://img.shields.io/cocoapods/v/RMRequest.svg?style=flat)](https://cocoapods.org/pods/RMRequest)
[![License](https://img.shields.io/cocoapods/l/RMRequest.svg?style=flat)](https://cocoapods.org/pods/RMRequest)
[![Platform](https://img.shields.io/cocoapods/p/RMRequest.svg?style=flat)](https://cocoapods.org/pods/RMRequest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RMRequest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RMRequest'
```

## Author

Rain, 2013752455@qq.com

## License

RMRequest is available under the MIT license. See the LICENSE file for more info.
