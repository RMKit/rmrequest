//
//  RMDefine.h
//  RMNetRequest
//  参考NerdyUI
//  Created by RainGu on 2021/4/19.
//

#import <objc/objc.h>
#import <objc/runtime.h>
#import <UIKit/UIKit.h>


static NSString * _Nullable const RMNetHttpStartEvent = @"RM.net.http.start";
static NSString * _Nullable const RMNetHttpEndEvent   = @"RM.net.http.end";
static NSString * _Nullable const RMNetHttpErrorEvent = @"RM.net.http.error";
static NSString * _Nullable const RMNetHttpLocalEvent = @"RM.net.http.local";

/*
 typedef
 */
typedef struct RMNERRect {
    CGRect value;
} RMNERRect;

typedef struct RMNERPoint {
    CGPoint value;
} RMNERPoint;

typedef struct RMNERSize {
    CGSize value;
} RMNERSize;

typedef struct RMNEREdgeInsets {
    UIEdgeInsets value;
} RMNEREdgeInsets;

typedef struct RMNERRange {
    NSRange value;
} RMNERRange;

typedef struct RMNERCallback {
    void * _Nullable targetOrBlock;
    SEL _Nullable action;
} RMNERCallback;

typedef struct RMNERFloatList {
    CGFloat f1, f2, f3, f4, f5, f6, f7, f8, f9, f10;
    CGFloat validCount;
} RMNERFloatList;


typedef NS_ENUM(NSInteger, RequestMethod) {
    RMRequestMethodGET = 0,
    RMRequestMethodPOST,
    RMRequestMethodHEAD,
    RMRequestMethodPUT,
    RMRequestMethodDELETE,
    RMRequestMethodPATCH,
};

typedef NS_ENUM(NSInteger, HttpRequestType) {
    HttpRequestDefault,//网络的请求
    HttpRequestUploadFile,//上传文件
    HttpRequestDownloadFile,//下载文件
};

typedef NS_ENUM (NSUInteger, NetResponseType){
    ///从网络请求数据成功
    RMResponseTypeSuccessNet,
    RMResponseTypeSuccessCache,///从缓存获取数据成功
    RMResponseTypeFailure = 10,
    RMResponseTypeFailureNet, ///网络请求失败，可能网络问题
    RMResponseTypeFailureNoMoreData = 1011,
    RMResponseTypeNoMoreData = 1037,
};

#define RMNER_MAKE_FLOAT_LIST(...)    ({RMNERFloatList floatList = (RMNERFloatList){__VA_ARGS__}; \
                                    floatList.validCount = MIN(10, RMNER_NUMBER_OF_VA_ARGS(__VA_ARGS__)); \
                                    floatList;})


typedef void (^RMNERSimpleBlock)(void);
typedef void (^RMNERObjectBlock)(id _Nullable );


#define RMNERNull             NSIntegerMax

#define Exp(x)              ({x;})


//@class RMNERConstraint;
//@class RMNERAlertMaker;
//@class RMNERStack;
//@class RMNERStyle;
//@class RMNERStaticRow;
//@class RMNERStaticSection;
//@class RMNERStaticTableView;

/*
 Chainable properties
 */

#define RMNER_READONLY                @property (nonatomic, readonly)

#define RMNER_PROP(x,y)               RMNER_READONLY RMNERChainable##x##y##Block
#define RMNER_CHAINABLE_TYPE(v, t)    typedef v *(^RMNERChainable##v##t##Block)

#define RMNER_LABEL_PROP(y)           RMNER_PROP(UILabel, y)
#define RMNER_BUTTON_PROP(y)          RMNER_PROP(UIButton, y)
#define RMNER_IV_PROP(y)              RMNER_PROP(UIImageView, y)
#define RMNER_TF_PROP(y)              RMNER_PROP(UITextField, y)
#define RMNER_TV_PROP(y)              RMNER_PROP(UITextView, y)
#define RMNER_SWITCH_PROP(y)          RMNER_PROP(UISwitch, y)
#define RMNER_PC_PROP(y)              RMNER_PROP(UIPageControl, y)
#define RMNER_SLIDER_PROP(y)          RMNER_PROP(UISlider, y)
#define RMNER_STEPPER_PROP(y)         RMNER_PROP(UIStepper, y)
#define RMNER_SEGMENTED_PROP(y)       RMNER_PROP(UISegmentedControl, y)
#define RMNER_EFFECT_PROP(y)          RMNER_PROP(UIVisualEffectView, y)
#define RMNER_IMG_PROP(y)             RMNER_PROP(UIImage, y)
#define RMNER_COLOR_PROP(y)           RMNER_PROP(UIColor, y)
#define RMNER_STRING_PROP(y)          RMNER_PROP(NSString, y)
#define RMNER_ATT_PROP(y)             RMNER_PROP(NSMutableAttributedString, y)
#define RMNER_CONSTRAINT_PROP(y)      RMNER_PROP(RMNERConstraint, y)
#define RMNER_ALERT_PROP(y)           RMNER_PROP(RMNERAlertMaker, y)
#define RMNER_STACK_PROP(y)           RMNER_PROP(RMNERStack, y)
#define RMNER_STYLE_PROP(y)           RMNER_PROP(RMNERStyle, y)
#define RMNER_STATIC_PROP(y)          RMNER_PROP(RMNERStaticTableView, y)
#define RMNER_SECTION_PROP(y)         RMNER_PROP(RMNERStaticSection, y)
#define RMNER_ROW_PROP(y)             RMNER_PROP(RMNERStaticRow, y)

/*
 Chainable types
 */

#define RMNER_GERMNERATE_CHAINABLE_TYPES(x) \
RMNER_CHAINABLE_TYPE(x, Empty)();\
RMNER_CHAINABLE_TYPE(x, Object)(id);\
RMNER_CHAINABLE_TYPE(x, TwoObject)(id, id);\
RMNER_CHAINABLE_TYPE(x, ObjectList)(id, ...);\
RMNER_CHAINABLE_TYPE(x, Bool)(NSInteger);\
RMNER_CHAINABLE_TYPE(x, Int)(NSInteger);\
RMNER_CHAINABLE_TYPE(x, TwoInt)(NSInteger, NSInteger);\
RMNER_CHAINABLE_TYPE(x, IntOrObject)(id);\
RMNER_CHAINABLE_TYPE(x, Float)(CGFloat);\
RMNER_CHAINABLE_TYPE(x, TwoFloat)(CGFloat, CGFloat);\
RMNER_CHAINABLE_TYPE(x, FourFloat)(CGFloat, CGFloat, CGFloat, CGFloat);\
RMNER_CHAINABLE_TYPE(x, FloatList)(RMNERFloatList);\
RMNER_CHAINABLE_TYPE(x, FloatObjectList)(CGFloat, ...);\
RMNER_CHAINABLE_TYPE(x, Rect)(RMNERRect);\
RMNER_CHAINABLE_TYPE(x, Size)(RMNERSize);\
RMNER_CHAINABLE_TYPE(x, Point)(RMNERPoint);\
RMNER_CHAINABLE_TYPE(x, Range)(RMNERRange);\
RMNER_CHAINABLE_TYPE(x, Insets)(UIEdgeInsets);\
RMNER_CHAINABLE_TYPE(x, Embed)(id, UIEdgeInsets);\
RMNER_CHAINABLE_TYPE(x, Callback)(id, id);\
RMNER_CHAINABLE_TYPE(x, Block)(id);

//RMNER_GERMNERATE_CHAINABLE_TYPES(UIView);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UILabel);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UIImageView);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UIButton);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UITextField);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UITextView);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UISwitch);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UIPageControl);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UISlider);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UIStepper);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UISegmentedControl);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UIVisualEffectView);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UIImage);
//RMNER_GERMNERATE_CHAINABLE_TYPES(UIColor);
//RMNER_GERMNERATE_CHAINABLE_TYPES(NSString);
//RMNER_GERMNERATE_CHAINABLE_TYPES(NSMutableAttributedString);


#define RMNER_CHAINABLE_BLOCK(x, ...) return ^(x value) {__VA_ARGS__; return self;}
#define RMNER_EMPTY_BLOCK(...)        return ^{__VA_ARGS__; return self;}
#define RMNER_OBJECT_BLOCK(...)       RMNER_CHAINABLE_BLOCK(id, __VA_ARGS__)
#define RMNER_INT_BLOCK(...)          RMNER_CHAINABLE_BLOCK(NSInteger, __VA_ARGS__)
#define RMNER_FLOAT_BLOCK(...)        RMNER_CHAINABLE_BLOCK(CGFloat, __VA_ARGS__)
#define RMNER_RANGE_BLOCK(...)        RMNER_CHAINABLE_BLOCK(RMNERRange, __VA_ARGS__)
#define RMNER_POINT_BLOCK(...)        RMNER_CHAINABLE_BLOCK(RMNERPoint, __VA_ARGS__)
#define RMNER_SIZE_BLOCK(...)         RMNER_CHAINABLE_BLOCK(RMNERSize, __VA_ARGS__)
#define RMNER_RECT_BLOCK(...)         RMNER_CHAINABLE_BLOCK(RMNERRect, __VA_ARGS__)
#define RMNER_INSETS_BLOCK(...)       RMNER_CHAINABLE_BLOCK(UIEdgeInsets, __VA_ARGS__)
#define RMNER_FLOAT_LIST_BLOCK(...)   RMNER_CHAINABLE_BLOCK(RMNERFloatList, __VA_ARGS__)

#define RMNER_TWO_INT_BLOCK(...)              return ^(NSInteger value1, NSInteger value2) {__VA_ARGS__; return self;}
#define RMNER_TWO_FLOAT_BLOCK(...)            return ^(CGFloat value1, CGFloat value2) {__VA_ARGS__; return self;}
#define RMNER_FLOAT_OBJECT_LIST_BLOCK(...)    return ^(CGFloat value, ...) {RMNER_GET_VARIABLE_OBJECT_ARGUMENTS(value); __VA_ARGS__; return self;}

#define RMNER_CALLBACK_BLOCK(...)     return ^(id target, id object) {__weak id weakTarget = target; __weak id weakSelf = self; __VA_ARGS__; weakTarget = nil; weakSelf = nil; return self;}


/*
 Introspect
 */
#define RMNER_TYPE(x)                 @encode(typeof(x))
#define RMNER_TYPE_FIRST_LETTER(x)    (RMNER_TYPE(x)[0])
#define RMNER_IS_TYPE_OF(x)           (strcmp(type, @encode(x)) == 0)

#define RMNER_CHECK_IS_INT(x)         (strchr("liBLIcsqCSQ", x) != NULL)
#define RMNER_CHECK_IS_FLOAT(x)       (strchr("df", x) != NULL)
#define RMNER_CHECK_IS_PRIMITIVE(x)   (strchr("liBdfLIcsqCSQ", x) != NULL)
#define RMNER_CHECK_IS_STRUCT_OF(x,y) ([[NSString stringWithUTF8String:x] rangeOfString:@#y].location == 1)
#define RMNER_CHECK_IS_OBJECT(x)      (strchr("@#", x) != NULL)

#define RMNER_IS_OBJECT(x)            (strchr("@#", RMNER_TYPE_FIRST_LETTER(x)) != NULL)
#define RMNER_IS_INT(x)               RMNER_CHECK_IS_INT(RMNER_TYPE_FIRST_LETTER(x))
#define RMNER_IS_FLOAT(x)             RMNER_CHECK_IS_FLOAT(RMNER_TYPE_FIRST_LETTER(x))
#define RMNER_IS_PRIMITIVE(x)         RMNER_CHECK_IS_PRIMITIVE(RMNER_TYPE_FIRST_LETTER(x))

#define RMNER_IS_STRUCT(x)            (RMNER_TYPE_FIRST_LETTER(x) == '{')
#define RMNER_IS_POINT(x)             RMNER_CHECK_IS_STRUCT_OF(x, CGPoint)
#define RMNER_IS_SIZE(x)              RMNER_CHECK_IS_STRUCT_OF(x, CGSize)
#define RMNER_IS_RECT(x)              RMNER_CHECK_IS_STRUCT_OF(x, CGRect)
#define RMNER_IS_INSETS(x)            RMNER_CHECK_IS_STRUCT_OF(x, UIEdgeInsets)
#define RMNER_IS_FLOAT_LIST(x)        RMNER_CHECK_IS_STRUCT_OF(x, RMNERFloatList)
#define RMNER_IS_BLOCK(x)             (x && [NSStringFromClass([x class]) rangeOfString:@"__NS.+Block__" options:NSRegularExpressionSearch].location != NSNotFound)



#define RMNER_SYSTEM_VERSION_HIGHER_EQUAL(n)  ([[[UIDevice currentDevice] systemVersion] floatValue] >= n)


#define RMNER_FIRAT_VA_ARGS(start, type) \
Exp(\
va_list argList;\
va_start(argList, start);\
type value = va_arg(argList, type);\
va_end(argList);\
value\
)

//#define RMNER_GET_VARIABLE_FLOAT_ARGUMENTS(start) \
//NSMutableArray *arguments = [NSMutableArray arrayWithObject:@(start)];\
//va_list argList;\
//va_start(argList, start);\
//CGFloat argument;\
//while ((argument = va_arg(argList, CGFloat)) != RMNERNull) {\
//    [arguments addObject:@(argument)];\
//}\
//va_end(argList);

#define RMNER_GET_VARIABLE_OBJECT_ARGUMENTS(start) \
NSMutableArray *arguments = [NSMutableArray array];\
va_list argList;\
va_start(argList, start);\
id argument = 0;\
while ((argument = va_arg(argList, id))) {\
    [arguments addObject:argument];\
}\
va_end(argList);




//http://stackoverflow.com/questions/2124339/c-preprocessor-va-args-number-of-arguments

#define RMNER_NUMBER_OF_VA_ARGS(...)  RMNER_NUMBER_OF_VA_ARGS_(__VA_ARGS__, RMNER_RSEQ_N())
#define RMNER_NUMBER_OF_VA_ARGS_(...) RMNER_ARG_N(__VA_ARGS__)

#define RMNER_ARG_N( \
_1, _2, _3, _4, _5, _6, _7, _8, _9,_10, \
_11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
_21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
_31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
_41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
_51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
_61,_62,_63,N,...) N

#define RMNER_RSEQ_N() \
63,62,61,60,                   \
59,58,57,56,55,54,53,52,51,50, \
49,48,47,46,45,44,43,42,41,40, \
39,38,37,36,35,34,33,32,31,30, \
29,28,27,26,25,24,23,22,21,20, \
19,18,17,16,15,14,13,12,11,10, \
9,8,7,6,5,4,3,2,1,0


#define WeakifySelf()       __weak typeof(self) weakSelf = self; weakSelf;
#define StrongifySelf()     typeof(weakSelf) self = weakSelf; self;


#define RMNER_SYNTHESIZE(getter, setter, ...) \
- (id)getter {\
return objc_getAssociatedObject(self, _cmd);\
}\
- (void)setter:(id)getter {\
objc_setAssociatedObject(self, @selector(getter), getter, OBJC_ASSOCIATION_RETAIN);\
__VA_ARGS__;\
}

#define RMNER_SYNTHESIZE_BLOCK(getter, setter, type, ...) \
- (type)getter {\
return objc_getAssociatedObject(self, _cmd);\
}\
- (void)setter:(type)getter {\
objc_setAssociatedObject(self, @selector(getter), getter, OBJC_ASSOCIATION_RETAIN);\
__VA_ARGS__;\
}

#define RMNER_SYNTHESIZE_BOOL(getter, setter, ...) \
- (BOOL)getter {\
return [objc_getAssociatedObject(self, _cmd) boolValue];\
}\
- (void)setter:(BOOL)getter {\
objc_setAssociatedObject(self, @selector(getter), @(getter), OBJC_ASSOCIATION_RETAIN);\
__VA_ARGS__;\
}

#define RMNER_SYNTHESIZE_INT(getter, setter, ...) \
- (NSInteger)getter {\
return [objc_getAssociatedObject(self, _cmd) integerValue];\
}\
- (void)setter:(NSInteger)getter {\
objc_setAssociatedObject(self, @selector(getter), @(getter), OBJC_ASSOCIATION_RETAIN);\
__VA_ARGS__;\
}

#define RMNER_SYNTHESIZE_FLOAT(getter, setter, ...) \
- (CGFloat)getter {\
return [objc_getAssociatedObject(self, _cmd) floatValue];\
}\
- (void)setter:(CGFloat)getter {\
objc_setAssociatedObject(self, @selector(getter), @(getter), OBJC_ASSOCIATION_RETAIN);\
__VA_ARGS__;\
}

#define RMNER_SYNTHESIZE_RANGE(getter, setter, ...) \
- (NSRange)getter {\
return [objc_getAssociatedObject(self, _cmd) rangeValue];\
}\
- (void)setter:(NSRange)getter {\
objc_setAssociatedObject(self, @selector(getter), [NSValue valueWithRange:getter], OBJC_ASSOCIATION_RETAIN);\
__VA_ARGS__;\
}

#define RMNER_SYNTHESIZE_STRUCT(getter, setter, type, ...) \
- (type)getter {\
return [objc_getAssociatedObject(self, _cmd) type##Value];\
}\
- (void)setter:(type)getter {\
objc_setAssociatedObject(self, @selector(getter), [NSValue valueWith##type:getter], OBJC_ASSOCIATION_RETAIN);\
__VA_ARGS__;\
}



