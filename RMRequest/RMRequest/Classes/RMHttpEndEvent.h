//
//  RMHttpEndEvent.h
//  RMNetRequest
//
//  Created by RainGu on 2021/4/21.
//

#import "RMBaseEvent.h"
#import "RMRequestMessage.h"
NS_ASSUME_NONNULL_BEGIN
@interface RMHttpEndEvent : RMBaseEvent

@property (nonatomic ,strong)RMRequestMessage * requstMessage;

@property (nonatomic)id httpRequst;

@property (nonatomic) id responsData;


@end

NS_ASSUME_NONNULL_END
