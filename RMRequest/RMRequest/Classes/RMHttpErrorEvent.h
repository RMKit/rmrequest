//
//  RMHttpErrorEvent.h
//  RMNetRequest
//
//  Created by RainGu on 2021/4/21.
//

#import <Foundation/Foundation.h>
#import "RMBaseEvent.h"
#import "RMRequestMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface RMHttpErrorEvent : RMBaseEvent

@property (nonatomic ,strong)RMRequestMessage * requstMessage;

@property (nonatomic)id httpRequst;

@property (nonatomic) NSError * error;

@end

NS_ASSUME_NONNULL_END
