//
//  RMHttpProtocol.h
//  RMNetRequest
//
//  Created by RainGu on 2021/4/21.
//

#ifndef RMHttpProtocol_h
#define RMHttpProtocol_h

#import "RMHttpEndEvent.h"
#import "RMHttpErrorEvent.h"
@protocol RMHandleEventDelegate <NSObject>

@optional

- (void)handleHttpEndEvent:(RMHttpEndEvent *_Nonnull)event;

- (void)handleHttpErrorEvent:(RMHttpErrorEvent *_Nonnull)event;

- (void)handleHttpLocalEvent:(RMHttpEndEvent *_Nonnull)event;

@end

#endif /* RMHttpProtocol_h */
