//
//  RMHttpRequest.h
//  RMNetRequest
//
//  Created by RainGu on 2021/4/20.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
//#import "RMRequestMessage.h"
#import "RMHttpProtocol.h"

//#import "RMHttpErrorEvent.h"
//#import "RMHttpEndEvent.h"
NS_ASSUME_NONNULL_BEGIN

#define Request [RMHttpRequest shareInstance]
#define Msg [RMRequestMessage new].xmethod(RMRequestMethodGET)

//返回数据block
typedef void(^RMHttpResponseBlock)(RMRequestMessage *request,id responseData,NSError * error);
//进度
typedef void(^RMHttpRequestProgress)(NSProgress * progress);


@class  RMHttpRequest;

@protocol  RMHttpRequestDelegate<NSObject>

@optional

- (NSDictionary *)requestHeaderFile:(RMHttpRequest *)request;

-(AFHTTPRequestSerializer *)requestSerializerFile:(RMHttpRequest *)request;

@required

@end

@interface RMHttpRequest : NSObject

@property(nonatomic , strong) AFHTTPSessionManager * manager;
//请求代理
@property (nonatomic , strong) id <RMHttpRequestDelegate> reqDelegate;
//数据处理代理
@property (nonatomic , strong) id <RMHandleEventDelegate> evnentDelegate;
//请求信息
@property (nonatomic , strong) RMRequestMessage *requestMessage;

@property (nonatomic , copy)   RMHttpResponseBlock responseBlock;

@property (nonatomic , copy)   RMHttpRequestProgress progressBlock;

+ (instancetype)shareInstance;

+(NSURLRequest *)getUrlRequest:(NSString *)route;
//设置请求头代理
+ (void)setRequestDelegate:(id)delegate;
/*
 注册数据处理(返回数据处理、错误数据处理、本地数据处理)
 */
+ (void)setHandleDelegate:(id)delegate;

/*
 请求方法
 @property message 请求参数
 @property responseBlock 返回Block
 */
+ (RMHttpRequest *)request:(RMRequestMessage *)message withResponseBlock:(RMHttpResponseBlock)responseBlock;

- (RMHttpRequest *)request:(RMRequestMessage *)message withResponseBlock:(RMHttpResponseBlock)responseBlock;

//带进度
+ (RMHttpRequest *)request:(RMRequestMessage *)message withProgress:(RMHttpRequestProgress)progressBlcok withResponseBlock:(RMHttpResponseBlock)responseBlock;

- (RMHttpRequest *)request:(RMRequestMessage *)message withProgress:(RMHttpRequestProgress)progressBlcok withResponseBlock:(RMHttpResponseBlock)responseBlock;


@end

NS_ASSUME_NONNULL_END
