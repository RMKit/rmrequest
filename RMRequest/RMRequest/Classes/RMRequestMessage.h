//
//  RMRequestMessage.h
//  RMNetRequest
//
//  Created by RainGu on 2021/4/20.
//
#import <Foundation/Foundation.h>
#import "RMDefine.h"
NS_ASSUME_NONNULL_BEGIN

@class RMRequestMessage;
@class uploadFile;

RMNER_GERMNERATE_CHAINABLE_TYPES(RMRequestMessage)

@interface RMRequestMessage : NSObject
///请求方式
//ER_PROP(KZBaseRequestMessage, Int) method;
@property (nonatomic, assign)RequestMethod  method;
///请求类型
@property (nonatomic, assign) HttpRequestType type;
///请求的参数
@property (nonatomic, strong) NSDictionary *params;
///请求头
@property (nonatomic, strong) NSDictionary *headers;
///请求的url 如route/route
@property (nonatomic, copy) NSString *route;
///缓存路径
@property (nonatomic, copy) NSString *cachePath;
///是否缓存
@property (nonatomic, assign) BOOL isCache;
///是否网络请求
@property (nonatomic, assign) BOOL isNotNet;
///是否模拟数据
@property (nonatomic, assign) BOOL isNotLocalData;
///请求body
@property (nonatomic, strong) NSData *body;
///请求所在的vc
@property (nonatomic, weak) UIViewController *vc;
///是否不展示错误视图
@property (nonatomic, assign) BOOL isCloseErrorView;
///是否不提示错误信息
@property (nonatomic, assign) BOOL isNoErrMsg;
//上传文件
@property (nonatomic, strong) uploadFile *upFile;
//下载存储路径
@property (nonatomic, copy) NSString *downFilePath;
/**
 *  多个文件上传
 *  fileSource存放字典，字典包含四个个key值kFILEDATA、kNAME、kFILENAME、kMIMETYPE
 *
 */
@property (nonatomic, strong) NSArray *files;
///上传图片
@property (nonatomic, strong) NSArray *imgs;
///类型
@property (nonatomic , assign) NSInteger targerType;


RMNER_PROP(RMRequestMessage, Int) xmethod;
RMNER_PROP(RMRequestMessage, Int) xtype;
RMNER_PROP(RMRequestMessage, Object) xparams;
RMNER_PROP(RMRequestMessage, Object) xheaders;
RMNER_PROP(RMRequestMessage, Object) xroute;
RMNER_PROP(RMRequestMessage, Object) xcachePath;
RMNER_PROP(RMRequestMessage, Object) xdownFilePath;
RMNER_PROP(RMRequestMessage, Bool) xisCache;
RMNER_PROP(RMRequestMessage, Bool) xisNotNet;
RMNER_PROP(RMRequestMessage, Object) xbody;
RMNER_PROP(RMRequestMessage, Object) xuploadFile;
RMNER_PROP(RMRequestMessage, Object) xvc;
RMNER_PROP(RMRequestMessage, Bool) xisCloseErrorView;
RMNER_PROP(RMRequestMessage, Bool) xisNoErrMsg;
RMNER_PROP(RMRequestMessage, Bool) xisNotLocalData;
@end

@interface uploadFile : NSObject
//文件
@property (nonatomic ,strong) NSData * fileData;
//服务器定义
@property (nonatomic ,copy) NSString  * Name;
//文件名
@property (nonatomic ,copy) NSString  * fileName;
//上传类型
@property (nonatomic ,copy) NSString  * mimeType;

@end

NS_ASSUME_NONNULL_END
