//
//  RMRequestMessage.m
//  RMNetRequest
//
//  Created by RainGu on 2021/4/20.
//

#import "RMRequestMessage.h"

@implementation RMRequestMessage

-(RMNERChainableRMRequestMessageIntBlock)xmethod{
    return ^(NSInteger para){
        self.method = para;
        return self;
    };
}

-(RMNERChainableRMRequestMessageIntBlock)xtype{
    return ^(NSInteger para){
      self.type = para;
       return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xroute{
    return  ^(NSString *para){
        self.route = para;
        return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xparams{
    return  ^(NSDictionary *para){
        self.params = para;
        return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xheaders{
    return  ^(NSDictionary *para){
        self.headers = para;
        return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xcachePath{
    return  ^(NSString *para){
        self.cachePath = para;
        return self;
    };
}

-(RMNERChainableRMRequestMessageBoolBlock)xisCache{
    return ^(NSInteger isCache){
        self.isCache = isCache;
        return self;
    };
}

-(RMNERChainableRMRequestMessageBoolBlock)xisNotNet{
    return ^(NSInteger isNotNet){
        self.isNotNet = isNotNet;
        return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xvc{
    return ^(id vc){
        self.vc = vc;
        return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xbody{
    return ^(id param){
        self.body = param;
        return self;
    };
}

-(RMNERChainableRMRequestMessageBoolBlock)xisCloseErrorView{
    return ^(NSInteger isNotNet){
        self.isCloseErrorView = isNotNet;
        return self;
    };
}

-(RMNERChainableRMRequestMessageBoolBlock)xisNoErrMsg{
    return ^(NSInteger isNotNet){
        self.isNoErrMsg = isNotNet;
        return self;
    };
}

-(RMNERChainableRMRequestMessageIntBlock)xisNotLocalData{
    return ^(NSInteger para){
      self.isNotLocalData = para;
     return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xuploadFile{
    return ^(id param){
        self.upFile = param;
        return self;
    };
}

-(RMNERChainableRMRequestMessageObjectBlock)xdownFilePath{
    return ^(id param){
        self.downFilePath = param;
        return self;
    };
}
@end

@implementation uploadFile
@end
